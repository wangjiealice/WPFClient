﻿using Microsoft.Practices.Prism.Commands;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Windows.Input;

namespace WPFClient
{
    public class EEPROM
    {
        public string mac { get; set; }
        public string sn { get; set; }
    }

    public class WPFClinetViewModel : INotifyPropertyChanged
    {
        public WPFClinetViewModel(IMessagingService messagingService)
        {
            _messagingService = messagingService;
        }

        private string _serverIP;
        private string _port;
        private string _mac;
        private string _sn;
        private HttpClient Client { get; set; }
        private readonly IMessagingService _messagingService;
        private string _value1;
        private string _value2;


        public string MACValue
        {
            get
            {
                return _value1;
            }
            set
            {
                _value1 = value;
                OnPropertyChanged("MACValue");
            }
        }

        public string SNValue
        {
            get
            {
                return _value2;
            }
            set
            {
                _value2 = value;
                OnPropertyChanged("SNValue");
            }
        }
        public string ServerIP
        {
            get
            {
                return _serverIP;
            }
            set
            {
                _serverIP = value;
                OnPropertyChanged("ServerIP");
            }
        }

        public string PortNumber
        {
            get
            {
                return _port;
            }
            set
            {
                _port = value;
                OnPropertyChanged("PortNumber");
            }
        }

        public string SN
        {
            get
            {
                return _sn;
            }
            set
            {
                _sn = value;
                OnPropertyChanged("SN");
            }
        }

        public string Mac
        {
            get
            {
                return _mac;
            }
            set
            {
                _mac = value;
                OnPropertyChanged("Mac");
            }
        }

        public ICommand LoadedCommand
        {
            get
            {
                return new DelegateCommand(OnLoadedRaised);
            }
        }
        

        public ICommand ConnectToServerCommand
        {
            get
            {
                return new DelegateCommand(OnConnectToServerRaised);
            }
        }

        //ClearCommand
        public ICommand ClearCommand
        {
            get
            {
                return new DelegateCommand(OnClearRaised);
            }
        }

        //SetCommand
        public ICommand SetCommand
        {
            get
            {
                return new DelegateCommand(OnSetRaised);
            }
        }

        private void OnLoadedRaised()
        {
            //string serverIP = @"http://" + ServerIP;

            //Client = new HttpClient();
            //Client.BaseAddress = new Uri(serverIP);
        }
        private async void OnSetRaised()
        {
            if(string.IsNullOrEmpty(ServerIP))
            {
                _messagingService.ShowMessage("Please input server IP!");
                return;
            }

            string serverIP = @"http://" + ServerIP;
            string url = serverIP + "/api/eeprom";

            if (Client == null)
            {
                Client = new HttpClient();
                Client.BaseAddress = new Uri(serverIP);
            }

            EEPROM value = new EEPROM { mac = MACValue, sn = SNValue };
            var putObject = JsonConvert.SerializeObject(value);
            HttpContent content = new StringContent(putObject, Encoding.UTF8, "application/json");
            HttpResponseMessage resp = await Client.PutAsync(url, content);
            if(!resp.IsSuccessStatusCode)
            {
                _messagingService.ShowMessage("Fail!");
            }
        }

        private async void OnConnectToServerRaised()
        {
            try
            {
                string Uri = @"http://" + ServerIP;
                string childUri = "/api/eeprom";

                Client = new HttpClient();
                Client.BaseAddress = new Uri(Uri);

                HttpResponseMessage response = await Client.GetAsync(Uri + childUri);
                if (response.IsSuccessStatusCode)
                {
                    string eepromJson = response.Content.ReadAsStringAsync().Result;
                    EEPROM prom = JasonToTConverter<EEPROM>(eepromJson);
                    Mac = prom.mac;
                    SN = prom.sn;
                }
            }
            catch (Exception ex)
            {
                _messagingService.ShowMessage(ex.Message);
            }
        }

        private void OnClearRaised()
        {
            Mac = string.Empty;
            SN = string.Empty;
        }

        private T JasonToTConverter<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string strPropertyInfo)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(strPropertyInfo));
            }
        }
    }
}